# How It Works

Our device is actually, in many ways, shockingly simple. It has lights (LEDs - light emitting diodes) that emit light at very specific wavelengths (fancy, science-y word for colors), which then bounce off objects (like carrots, or carrot pulp, or spinach, or soil). Some of it is absorbed (turned into other forms of energy like heat) by the object, and then a light sensor in the device reads how much light bounces back (for each wavelength, multiple times, very quickly).

![image](img/how-it-works.png)

Why this matters (light bouncing off) is because this is actually a characteristic of objects that is directly correlated to the chemical compounds making up that object. So in the case of food, there are known correlations between light reflectance, at specific wavelengths, and the amount of different nutrients found in that food (vitamins, antioxidants, and aromatic compounds, to name a few), the levels 
or organic carbon in soil, and chlorophyll content in plants, etc. 

What makes this extra complicated though, is that these light-bouncing characteristics and compounds “overlap” with each other — so we need to look at lots of data to try to parse out what is causing the response we’re seeing.

**This is why we’re working with first users to collect data for calibration purposes, before making this a commercial device.**