# Getting Started with the Our-Sci App

## Our-Sci Apps
  - Our SciKit ([link](https://play.google.com/store/apps/details?id=org.oursci.android.ourscikit))
  - RFC Collect ([link](https://play.google.com/store/apps/details?id=org.oursci.android.bionutrientmeter))
  - QuickCarbon ([link](https://play.google.com/store/apps/details?id=org.oursci.android.quickcarbon))

  The following `Mobile App` help articles apply to all of these apps

Here is a video tutorial about downloading and setting up an OurSci app: 
<iframe width="560" height="315" src="https://www.youtube.com/embed/bkpzeHUJ7G8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 




## Important Definitions

|Term|Definition|
|----|----------|
|Form|Document which defines all the questions in a survey, written in XML.|
|Survey|Once you start filling out a form, you have created a survey|
|Dashboard|Data visualization tool for survey results|
|Script|Javascript code that either runs an instrument or calculates results from data in a survey or other data available on the web|
|Organization|A group of users that are using a common set of forms|
|ODK Build|Web-based tool for creating forms| 





## How the Platform works

1. A data collection `Form` is created 
     - `Forms` are created using [ODK Build](https://build.opendatakit.org/)
     - `Forms` are then uploaded to the [web app](https://app.our-sci.net/#/)
2. Open or download the `Form` on an **Our Sci App** 
     - Collect data and run measurements and scripts using Our Sci compatible instruments
     - Data can be collected without an internet connection, stored on the android device and then submitted to the web app later
     - See below for a list of Our Sci mobile apps
3. View your data on the web app
     - View your data using customizable dashboards
     - Download your data as a csv