# Tech Specs

### Key Features

- 10 wavelength reflectometer output not affected by ambient light
- Robust and field-ready
- Connects to Our-Sci app (Survey Stack) to collect, share, and display results
- Connects to any Android device wirelessly using Bluetooth or wired USB On The Go
- Includes a wall charger and an internal 3400 mAh battery
- Fully open source and hackable

### Applications 

**Designed for use with:**

- liquids in 5 mL or 0.7 mL square cuvettes
- liquid colormetric measurements (example: pH change)
- cell density (940 nm)
- examples: milk fat concentration; spectral properties of juices, wines, etc.
- soil or other bulk soil
- soil carbon estimation (requires calibration)
- soil color estimation
- solid objects (fruits, veggies)
- spectral signatures for nutrient estimation (requires calibration)
- leaves or other flat objects
- NDVI (greenness) in leaves
- relative carotenoids, anthocyanins, or other color-related compounds

### Default Output

- Reflectance at all wavelengths (normalized from 0 - 100)
- Chlorophyll fluoresence and any fluorescence with excitation <700 nm and flurorescence >700 nm
- NDVI (greenness) in leaves

### Sensor Detail

**Reflectance hardware:** 

- 10 wavelength (nm): UVB - 365; Visible - 385; 450; 500; 530; 587; 632; Far Red; NIR - 850; 880; 940
- 2 detectors (nm range): 300-700; 700-1100

**Additional Sensors:** 

- Temperature (full range: -40 - 85 C, accuracy within 0 - 65 C: +/- 1C)
- Relative Humidity (range: 0-100%, accuracy: +/-3%)
- Pressure (range: 300-1100 hPa, accuracy: +/-0.25%)
- VOCs (range: 0 - 500 IAQs, accuracy: +/- 15%)
- Audio jack for additional inputs / outputs. Examples include soil senseors, moisture sensors, etc.

### Case
- 13.9 cm x 5.2 cm x 4.6 cm
- 3D printed PETG
- Temperature resistant up to 60 C

### Calibration

All instruments come individually calibrated to reduce instrument-to-instrument variation, variaton by time and by temperature.

For long-term shifts in signal intensity, users may easily recalibrate, or calibrate several machines to the same standard.