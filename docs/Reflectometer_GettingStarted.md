# Getting Started with Our-Sci 

## Join Our Weekly Help Sessions

**We know that getting started with the Our-Sci app and devices can be overwhelming. If you’ve received your device and are not sure how to begin, or would just like to see it done first, check out our weekly help sessions. You can join at [this link](https://yale.zoom.us/j/93056414493) on Wednesdays from 3-4 pm EST to get your questions answered.** 

If you can't make it at this time, feel free to reach out to nat@our-sci.net to ask your question over email, or schedule a meeting at a time that works for you.

## Useful Pages within the Our-Sci Documentation & Tutorials

**In addition to help sessions, you can follow the resources below for step-by-step processes covering setting up the Our-Sci App app on your Android phone, connecting your device via bluetooth, contributing to organizations, and much more.** 

### Getting Started on the Our-Sci App:

- [App Overview](https://nat-irwin.gitlab.io/mkdocs/Getting_Started/)

### Getting Started with Organizations:

- [Organizations](https://nat-irwin.gitlab.io/mkdocs/Organizations/)

- [Contributing to an Organization](https://nat-irwin.gitlab.io/mkdocs/Contributing_to_an_organization/)

### Getting Started with Forms & Surveys:

- [Creating a Form](https://nat-irwin.gitlab.io/mkdocs/Creating_A%20_Form/)

- [Choosing a Form](https://nat-irwin.gitlab.io/mkdocs/Choosing_a_form/)

- [Searching for & Submitting Surveys](https://nat-irwin.gitlab.io/mkdocs/Searching_for_%26_Submitting_Surveys/)

### Getting Started with the Our-Sci Reflectometer:

- [Connect to an Instrument](https://nat-irwin.gitlab.io/mkdocs/Connect%20to%20an%20instrument/)

- [Calibration Procedure](https://nat-irwin.gitlab.io/mkdocs/Calibration_Procedure/)

### Resources Coming Soon

'Getting Started Video' -- Coming Soon! 

'Getting Started Packet' -- Coming Soon! 