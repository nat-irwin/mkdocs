# Contributing to an organization

One advantage of the **Organization** feature is that it allows you to filter out all other forms on the **OurSciKit** app so that you only see your organization's `Forms`

In order to use this feature:

  - An organization needs to be created using the [web app](https://app.our-sci.net/#/)
  - The forms used by your organization need to be **Favorited**
  - The members who have permission to contribute to your organization have to be added on the web app 

## Selecting an organization in the app

From the home screen (below left)

1. From the apps home screen, select `CONTRIBUTE TO ORGANIZATION`
2. Select the organization for which you are contributing data.
3. Hit the back button

From the main menu (below right)

1. From the main menu, select your avatar
2. Select the organization for which you are contributing data.
3. Hit the back button

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/ab504eb7af69bc36e43ce5d8f15f1b6e/image.png)