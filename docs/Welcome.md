# Welcome to Our-Sci Tutorials

This page houses Our-Sci tutorials and documentation to help users get started with our products. You can expect to find content for getting started with the Our-Sci app, joining an organization, designing surveys and research projects, and setting up and using your reflectometer.

Please see the sidebar on the left for a full list of content.

Here is a video tutorial to get you started: 
<iframe width="560" height="315" src="https://www.youtube.com/embed/lfCWhF9aFSw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>