# Searching for & Submitting Surveys

## Searching for a survey

Surveys are stored locally on Our Sci apps (OurScikit, RFC Collect, QuickCarbon) until the users choose to delete them. This way users have the flexibility to start a survey and come back to complete it later or to change the answers and resubmit a survey that they have already submitted.

To search for surveys stored locally on the app:

1. Navigate to `Surveys` from the Apps main menu
2. Scroll through the surveys to find the survey you wish to submit **OR** search for the survey.
3. To search, select the **search icon** from the top menu.
4. Enter a keyword from the survey of interest into the text box (for example, the sample ID) **OR** you can press the **QR code** icon and scan a QR code to search for the survey.
5. The search results will appear in the `Surveys` page.

***Note:*** There are two tabs in the `Surveys` page, `ACTIVE` AND `SENT`, make sure to check **Both** tabs for the survey you are searching for.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/3dbdf8123e9f6d56814b06911820f929/image.png)

## Submitting a survey

The data from Our Sci surveys (OurScikit, RFC Collect, QuickCarbon) is hosted on the [Our Sci online platform](https://app.our-sci.net/#/). You can submit surveys at the end of the survey or from the `Surveys` page. You can also re-submit a survey that you have already sent, if you need to revise an answer (or measurement) or if you submitted the survey before it was complete.

### When you complete the survey
On the last page of every survey is a review page. This page will display a check mark for every completed answer. 

If you have completed the survey and are ready to submit it to the web platform, select `SEND`.
 
If you are not finished, or you are not ready to submit the survey, select `DONE` and you can submit the survey later.

![Send_survey](https://gitlab.com/our-sci/documentation/-/wikis/uploads/e87fb23e949c31cd5ee74c24eb8c46e0/Send_survey.jpg)

### From the Surveys tab
You can also submit a survey from the `Surveys` tab:

1. Navigate to `Surveys` from the Apps main menu
2. Scroll through the surveys to find the survey you wish to submit (you can also search for your survey in the app, see the **Searching for a survey** tutorial above).
3. Select the survey you wish to submit by pressing and holding that survey until it is selected. When selected you will see a check in front of the survey.
4. Select the Upload to Cloud icon from the top menu bar.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/d103bb2d3b1109b751880eda554e1c81/image.png)


### Re-submitting a survey
All surveys are stored on the app unless you delete them. So, if you accidentally submitted an unfinished survey, or need to revise an answer on a completed survey, you can re-submit that survey.

1. Access the survey that has already been submitted, by searching for the survey in the `SENT` tab, at the top of the `SURVEYS` page. 
2. Tap on the survey to open up that surveys **Review Page**
3. Navigate to the entry (question or measurement) that you need to complete or revise. If you are re-running a measurement you will need to select `REDO` from the bottom of the screen.
4. Navigate back to the **Review Page** and select `RESEND`.
5. A pop-up dialog will ask you to confirm that you would like to resend the survey.

**Important Note:** Re-submitting a survey will over-write the survey that was previously sent, so make sure that all of the information is correct before re-sending.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/8d9f4cc951a146583e31376463a94de8/image.png)