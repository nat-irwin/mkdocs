# Overview

![image](img/spectralclosebetter.png)

### What is it? / What can it do?

“But wait,” you say, “there are already some out there, and they are pretty well designed and reasonably 
priced!”  Well, yes – there are full spectrometers like the [Spectruino ($411)](https://myspectral.com/),
the [Open Source Colorimeter ($80 + $20 per LED)](https://iorodeo.com/collections/open-source-colorimeter/
products/educational-colorimeter-kit?variant=12262607558) from IORodeo, and publications from universities 
describing open colorometer designs [(Appropedia and MTU have a good one](https://www.appropedia.org/
Open-source_colorimeter), but there are several others – these are DIY so < $100 in parts). Pretty cheap, 
and lots of available designs.

Like the [seat designed for the average person but usable by no one](https://www.thestar.com/news/insight/
2016/01/16/when-us-air-force-discovered-the-flaw-of-averages.html), product designers should avoid the law 
of averages.  As in that case, the aforementioned devices are too general purpose to be particularly 
useful. [The MultispeQ ($600)](https://photosynq.org/users/sign_in) could work, but was designed for 
photosynthesis measurements and is over-designed for applications outside of photosynthesis. For [our 
community partners](http://blog.our-sci.net/people/), none of these devices do exactly they 
need, which is…

- Arborists need a low cost and easy to use chlorophyll meter to add more rigorous sensor data to      visual tree assessments.

- Consumers + farmers need a way to measure food nutrient density in stores and on farms.

- Soil scientists and regulators need to measure soil carbon in the field, quickly and easily.  Doing so could create a massive new pathway for carbon markets to value sequestration of carbon in soil.

- Cannabis growers, consumers, and dispensaries need to be able to confirm total cannabinoids and THC levels to comply with regulations, ensure quality product, and identify fraudsters.

These cases require a device which is low cost, easy to use by non-scientists, flexible in what 
they measure (drops of liquids, cuvettes, leaves, aggregate solids like soil, and whole solids like a 
pear), usable in field conditions, fast, and open source.  

Our core design is based on the open source MultispeQ (a photosynthesis measurement device), which uses 
LED supplied light sources at 10 different wavelengths, but is much lower cost.  While this isn’t a full 
spectrometer, it has the advantage of working independent of ambient light (unlike a normal spectrometer 
or simple colorimeter where the sample must be in darkness) while being relatively inexpensive (cheaper 
BOM and less time/cost to make/calibrate).

Ideally, we want users to be able to measure soil carbon, leaf chlorophyll content, brix from extracted 
sap, and the density of a pear fruit all at the same time with the same instrument.  This not only reduces 
the cost and increases utility, it also spreads our development costs across multiple applications.  Our 
design accommodates all of these uses.

### Crowd-Sourcing Data

This device was engineered to be a tool with a wide range of uses, some of which have not been studied yet. In order to be accurate for its intended use, it requires reference data for use-specific prediction models.

Reflectance is a pretty simple measurement and tells you almost nothing without reference data. Reference data measures reflectance values and validated lab-based measurements on the same set of samples to build correlations between the two (if they exist!). But building a reference database can be very expensive. In the case of food nutrition, measuring a small suite of lab tests for vitamins, minerals and antioxidants can cost $500 or more. A reference database might contain 100s or 1000s of measurements to have sufficient predictive power. Yikes!

As a community-oriented organization, we are looking for a community-driven solution to developing use-specific prediction models. Right now, we’re looking for help in creating our reference data sets. We need users who are willing to collect data to build out these data sets on a broad enough scale to be useful for predictive models. These models will get our device to a stage appropriate for commercial use. 

Out of the box, this device can measure chlorophyll content in plants.

With predictive modeling, we have this seen that this device can measure: 

- Soil organic carbon (SOC) in soil

- Milkfat content in milk

- Starch levels in produce

- Relative nutritional content in food

Got other ideas? Great! The possibilities are wide open. We’ve set up forums for the 
community to discuss their thoughts, discoveries, challenges, and insights.

### Our Mission

We strive to support the development of research capacity in communities through software, hardware, and training.  Specifically, we want to help turn citizens into researchers, researchers into community leaders, and communities into solvers.

With our low-cost tools, we aim to:

- Help communities disrupt the systems that they think need disruption
- Improve data accessibility across a diverse set of users
- Increase the scientific rigor of community-based projects and decision-making
- Foster a network of communication, trust, and independence
- Be transparent and independent in everything we do

**We have accomplished proof of concept on all of these objectives for multiple use cases, and we are now readying ourselves for full implementation.**  

**With the help of our partners and crowd-sourced data projects, we expect to have our first consumer-ready tools available with basic definitions of crop and soil quality and guidance for growers by next year!**