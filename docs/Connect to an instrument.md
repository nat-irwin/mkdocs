# Connect to an instrument
A video tutorial for connecting an instrument can be found 
<iframe width="560" height="315" src="https://www.youtube.com/embed/ETCJXxs-_B0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Turning on the Reflectometer or Bionutrient Meter
  - The device must be turned on before it can connect to an android app
  - Press and hold the **Power button** for ~3 seconds
  - The green **Power indicator light** will come on if the device is on
  - The device will automatically turn off after 5 minutes of inactivity

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/e5d3fbde4e1b9c3ff5b34e2b3a98ca70/image.png)

### Pairing an instrument
The first time you connect your Bionutrient meter (or other Our Sci compatible instrument) to an Our-Sci app (OurScikit, RFC Collect, QuickCarbon) you will need to **Pair** your device. 

1. You can pair your device by:
     - Clicking on the instrument icon in the top menu bar.
     - Once you reach a point in the survey that requires running a measurement, select the button labelled `CONNECT TO DEVICE`.
2. Select `START SEARCH`. 
3. Once your Bionutrient Meter appears in the list (the **Device ID** in the list will match the **Device ID** on your meters lanyard) select `PAIR`. 
4. A **Pairing Device** dialog box will pop up, select `START PAIRING`.

    ***Note:***  For some phones, the **Pairing Device** pop up will appear in the phones `Notifications` as a `Pairing Request`. If the **Pairing Device** dialog box does not appear automatically, pull down the phones notifications and tap on the `Pairing request`.

5. You will need to enter a code to pair with your device. The code for all devices is ***1234***
6. Once the app is paired to the device, it will show up as **CONNECTED to XX**.
7. Select `BACK` to return to the app

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/b2bfd4d4da734b8637cb6fbec50f229e/image.png)
![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/5b26b26cdc37c8a0755187038bfe0bb4/image.png)
![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/fed2fb86b9b8d87aaabef86cb02abb25/image.png)


### Connecting to an already paired device

You can connect to your device by:

1. Once you reach a point in the survey that requires running a measurement, select the button labelled `CONNECT TO DEVICE`

    - If it works properly, the device will connect and the button will switch to `RUN MEASUREMENT`.
    - If it does not work properly, make sure the the Bionutrient meter is turned on and try again.  
    - If it still does not work, go to the `DEVICE LIST`.   

2. Click on the instrument icon in the top menu bar or the `DEVICE LIST` button

    - The app will remember the last device that was connected. If you do not see your device, select `START SEARCH`.
    - The device list will help you identify if you need to **DISCONNECT** from another device or if you need to **PAIR** with the device before connecting.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/c21cee7f35fffd6cf8bd61bd4b268361/image.png)