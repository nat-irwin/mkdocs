# Welcome to Our-Sci Tutorials

This page houses Our-Sci tutorials and documentation to help users get started with our products. You can expect to find content for getting started with the Our-Sci app, joining an organization, designing surveys and research projects, and setting up and using your reflectometer.

## About Our-Sci

### Motivation
**Our research institutions are disconnected from the public they serve…**  Research today is driven by industry, government, and academia. These institutions have an important place, but they should not be the only place for science and research. They tend to produce expensive, closed, large-scale solutions which are not responsive to the values of the researchers doing the work or the needs of the communities benefiting from it.

**Communities don’t believe they are capable of doing research and development…**  We failed to realize that science – the process of experimentation and a culture of scientific rigor – is not the sole domain of academia and industry. Communities do not need to wait in line and pay too much for someone else to solve their problems.

**But the traditional barriers to science are gone…**  Information is accessible, communication is borderless, sensors are cheap and the sharing economy is quickly becoming the new normal. We can now connect people + sensors to tackle both local and global problems.

**And the benefits are both deep and wide…** Students and teachers are looking for meaningful learning opportunities, researchers want to analyze global data sets, and individuals are seeking the tools to pursue their interests and collaborate at scale. And we all want to reap the huge long-term benefits from our shared data.

**So if we do this right…** Accessible + welcoming to everyone, producing high-quality comparable data, building robust communities of practice, and creating a culture of sharing. Equitable and collaborative. With software, hardware, and data which is free as in freedom.

**We can take back science…** And drive positive change in the science that already exists.

### Mission Statement

We strive to support the development of research capacity in communities through software, hardware, and training.  Specifically, we want to help turn…
- Citizens into researchers,
- Researchers into community leaders,
- And Communities into solvers.

### Values
- Openness at all levels (free and open source hardware, software, data, and finances)
- Accessibility (diversity of users, # of users)
- Scientific rigor (comparability, validation, quality)
- Communication (between ourselves and our clients)
- Trust (between individuals, communities, and us)
- Independence (of individuals, communities, and us)

## What We Do

**We support COMMUNITY-DRIVEN research**

Any community is capable of solving even the hardest problems using their own skills and resources.  The cornerstones of successful community research are comparability of data and methods, validation of tools and people, scientific quality instrumentation, and a collaborative and equitable process.

**We combine TECH & KNOW-HOW**

When modern communities and organizations tackle complex problems, one-size-fits-all solutions just don’t work.  We have staff with deep domain knowledge to speak your language and identify the best solution.

**We help SIMPLIFY your process**

Starting and scaling data collection, analysis and distribution is hard. There are lots of tools to tackle parts of the process, but almost none that do them all efficiently, inexpensively, flexibly, and easily.  We are a one-stop shop to support your data flow needs with experience, open source software and hardware, and in-house design/visualization support.

### Tools

**SurveyStack**

SurveyStack is our in-house data collection, management, and analysis platform, designed with community-driven research in mind.

**Project Management and Tracking**

We use Gitlab for issue tracking and notes, custom dashboards for tracking complex processes like sample flows or member onboarding, and mkdocs for scalable, collaborative documentation.

**Reflectometer**

The Our Sci Reflectometer is a general purpose, robust 10 wavelength device for measuring spectral reflectance from liquids (eg. milk), flat objects (eg. leaves) or solids (eg. soils, produce).


### Project Example

We work on a variety of projects. To get a sense of the type of work we support, check out this introduction to our partnership with the Real Food Campaign:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lfCWhF9aFSw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>