# Choosing a form

## Downloading a form for the first time
If you are choosing a form for the first time, you will need to download the form to the app.

1. Go to the apps main menu
2. Select `Forms`
3. Select the form you want to from the list.

**Important Note:** In the **RFC Collect App**, all of the forms are downloaded automatically to the `Forms` tab so you do not need to download them and there are no `Offline Forms`

## Opening a form that is already downloaded
If you have already downloaded the form, then you can open it from the `Offline Forms` tab.

## Updating a form in the app
If a form has been updated on the web app, then you will need to download the changes to the app before creating a new survey. To do this, go to the `Forms` tab and choose the form.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/44535a9df974afb03f58c98c464be688/image.png)