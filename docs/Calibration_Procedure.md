# Calibration Procedure

**If you are using the device in your own professional or research application, you should plan to run a calibration directly prior to running the unit.**

## You will learn

* When to run a calibration
* How to calibrate
* How to know a calibration was good
* Troubleshooting
* Technical details: What a calibration does
* How to get help

## When to run a calibration
* At the beginning of days when you collect data (a field collection day for example)
* After a significant change in environmental conditions or temperature.
* NOTE: Calibrations should be run in the same environmental conditions, especially similar temperatures, to the actual samples you’ll be running.  In other words, don’t calibrate in a cold basement, and then go to a hot field and perform all of your actual measurements.

## How to Calibrate
In the Our SciKit app, go to the main menu by clicking on the 3 lines in the upper left.  Then click on Start Survey (Online) and search for “Range Calibration 2”.  Answer ‘no’ to the original manufacturer question and then follow the directions.  Important tips for a good calibration are shown below:

* Always clean the surface of the instrument with a new paper towel or other clean, non-scratchy cloth.  A small amount of dust or sticky plant material will reflect light, impacting your calibration.

![image6](https://gitlab.com/our-sci/documentation/-/wikis/uploads/f2b0743eae5715ced8c503f90ac97279/image6.jpeg)

* Check the device clamp and make sure it’s not dirty.  If it is, you can clean it with a slightly moistened cloth.

![image8](https://gitlab.com/our-sci/documentation/-/wikis/uploads/f9813dca636c125e871fe8271c0756be/image8.jpeg)

* During the Open Blank step, open the clamp and aim the device into a relatively open space like this.

![Image_from_iOS](https://gitlab.com/our-sci/documentation/-/wikis/uploads/edf4bac41d1b84b22fc44338f9d8bfed/Image_from_iOS.jpg)

* During the Teflon Standard step, start by unscrewing the Teflon block from its stored location at the back of the device. When put in the clamp for calibration, the Teflon Standard should be facing the correct direction, and you should ensure the clamp is aligned with the magnets and pressed firmly against the surface of the instrument.  There should be no large gaps between the Teflon standard and the instrument!

![image3](https://gitlab.com/our-sci/documentation/-/wikis/uploads/5cece0b83647d255504a59634cd1d04c/image3.jpeg)**CORRECT ALIGNMENT OF TEFLON**

![Image_from_iOS2](https://gitlab.com/our-sci/documentation/-/wikis/uploads/bc322110275fc98dd8b02705bc3e8d58/Image_from_iOS2.jpg)**GOOD ALIGNMENT OF TEFLON AGAINST SURFACE**

![image5](https://gitlab.com/our-sci/documentation/-/wikis/uploads/29068a8dd658831aa2ccdf3c9f8cb21c/image5.jpeg)**POOR ALIGNMENT OF TEFLON AGAINST SURFACE**

## Confirming the calibration was good

Once the calibration was complete, you will be prompted to confirm the calibration.  This simply checks to see if the calibration was good.  You should repeat the calibration steps.  Some tips at this stage are shown below:

* The Open Blank confirmation should produce a graph of values between -100 and +100.  If values are outside this range, you should re-do the calibration.

![image0](https://gitlab.com/our-sci/documentation/-/wikis/uploads/dc72f3776b90ebade735fbf688eda991/image0.jpeg)
![image1](https://gitlab.com/our-sci/documentation/-/wikis/uploads/608ead22399839b047eaf0fffe08bff9/image1.jpeg)

* The Teflon Standard confirmation should produce a graph of values between 9900 and 10100.  If values are outside this range, you should re-do the calibration.

![image31](https://gitlab.com/our-sci/documentation/-/wikis/uploads/46a3ab3f52e3000956ab66a03ff009c8/image31.jpeg)
![image42](https://gitlab.com/our-sci/documentation/-/wikis/uploads/2dbe61110b176f8800e5b62b1373cea8/image42.jpeg)

* If the calibration was good, then complete the survey and hit the submit button.  This will help you track the quality of this instrument over time and help anyone troubleshoot issues if you have them in the future.

## Re-doing a calibration

To re-do a calibration, simply exit the current calibration and create a new survey, performing the calibration again until it is complete.

## Troubleshooting

1. I can’t get my calibration to consistently achieve 0 for the blank and 10,000 for the Teflon
   * Usually, repeatable physical positioning of the Teflon standard against the instrument is the problem.  It should sit flush against the surface, and the clamps magnets should be lined up with the magnets on the instrument.  Also check for a clean surface.  Doing the same thing every time is the most important - so try to be repeatable!
2. I can’t find the “Range Calibration 2”.
   * Make sure you are connected to the internet first by trying to go to google.com or another common site through the browser.  If you are connected, check your spelling.

## Technical Details: What a calibration does

**The calibration helps ensure a device is comparable to itself over time**

The reflectometer uses 10 LEDs at 10 wavelengths in order to see how reflective the sample is at each wavelength.  The problem is that over time, the reflectivity for the exact same sample can drift over time.  Some reasons for this drift are:

* LEDs producing less light due to time or use
* The light detector changing over time
* Fading or scratching of the light guide which can change the amount of light hitting the sample or being received.
* Changes in the electronics which supply the power to drive the LEDs, causing the LED to produce more or less light.

In order to ensure that a single device is comparable over time, we calibrate that device to a standard set of objects for which the reflectance is not changing, and recalibrate the device to ensure those objects always return the same value.

In our case, we are calibrating to two objects: a blank (empty space) and a Teflon standard (a highly reflective space).  This device uses very brief pulses of light and subtracts off any background light - making ambient ‘constant’ light sources (and even flickering sources which are lower frequency) effectively ‘invisible’ to the instrument.  As such, pointing the instrument into a empty room, even a lit room, returns effectively zero detectable light back to the detector.  This ‘empty room’ is the cheapest and most universal blank we can find!

As such, **we assign a value of 0 to the results of the Open Blank measurement** for each wavelength.

The Teflon standard is universally reflective in the wavelength range we work in, so it is more or less the brightest thing we can find.

As such, **we assign a value of 10,000 to the results of the Teflon Standard measurement** for each wavelength.

So, even if the LEDs become weaker or there are scratches on the unit, we can be sure that objects will always report the same reflective values because we have known low and high points of reference.

**What about comparability between devices?**

Device to device comparability is supported by calibration, because devices can always be traced back to their calibration standard, but it is not a full solution for device to device comparability.  In order for all devices to be perfectly comparable over time, they would need to be traceable back to a single, common standard, like a single common piece of Teflon, which they are not.

**If you need strong comparability between devices, you should post your specific application to the forum and we can discuss ways to get improved results.**

## How to get help

Post your issues on the [forum at forum.goatech.org/c/our-sci](forum.goatech.org/c/our-sci).  This will make sure we can find the best person to support you!