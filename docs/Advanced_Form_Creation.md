# Advanced Form Creation

## Anonymization

The Our Sci platform is designed for open data and sharing. However, depending on your application, some data may need to be kept confidential (For example, the names and email address of participating farmers). 

Using the anonymization feature allows you to keep sensitive information private while keeping the majority of the data collected public.

### Who can see anonymized data?
  - The person who submitted data can always see the anonymized data that they submitted.
  - If the data collection `Form` is part of an **Organization**, then anyone who is listed as an `Administrator` of that organization can see anonymized data.


### How to create a Form for collecting anonymized data.

1. Create a new form in [ODK build](https://build.opendatakit.org/), if you have never created a form, check out our tutorial [here](https://nat-irwin.gitlab.io/mkdocs/Creating_A%20_Form/)
2. Select `Group` from the bottom menu.

    **Important note:** Data can only be anonymized as part of a group. 

3. In the `Properties` sidebar menu (on the right), give your group a `Data Name`
4. Enter a `Label`, this will be the text that the user will see in the mobile app.
5. The `Label` text needs to be followed by a `;` and then `anon` with no spaces. For example: `Site specific information;anon`
6. Add the questions to the form that you want anonymized and drag and drop those questions into the `Group`.
7. Once you have completed the form, [upload the form to the web app](https://gitlab.com/our-sci/documentation/wikis/creating-a-form)

**Note** Only add the questions that you need anonymized to the `Group`

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/f1dd53dbeee5b1bf7b5d2854700f80a9/image.png)

### View in the app
You can decide whether or not to anonymize the data on the 1st page of the `Group`. In the example below, we see the group label `Site specific information` and the `Anonymize` checkbox is checked. Therefore, the answers to all of the questions in that group will be anonymized. You can choose not to anonymize the data by unchecking the `Anonymize` box. ***Note:** The checkbox is checked by default.*  


![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/056391c5952cf53456cb9333d11d75ac/image.png)


## Adding Conditional Statements

The ODK-Relevance attribute is supported by the Android application and included in the skip logic.

Following example demonstrates how it may be used:

```
(selected(/data/cover_crop_usage, 'Yes'))
```

`/data/<id>` refers to the question, `Yes` to the selected value.

If you are using relevance within a group use:
```
(selected(/data/group_id/cover_crop_usage, 'Yes'))
```
`/data/<group>/<id>` refers to the group and then question, `Yes` to the selected value.

More in-detail guide: [https://docs.opendatakit.org/form-logic/](https://docs.opendatakit.org/form-logic/)


To link a whole group of questions to a previous answer:

1. In the group screen, go to `advanced`

2. Enter the relevance logic (see above)

3. All the questions in the group will be shown or hidden depending on that logic.

To link a specific question to a previous answer:

1. Open the `advanced` options for the question

2. Enter the relevance logic for that question

3. The relevance logic will only apply to that question.  

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/06682a40efe3cbc3b4b5df06986bbc6a/image.png)

## Adding an Ontology 

### Adding an ontology list as an auto-suggest drop down menu

Form creators can add an ontology list to a form, so that as soon as users select the text box and start typing an auto-suggest drop-down list will appear.

1. Select the **Text** question type in the **ODK Build** form builder.
2. In the **Hint** field add your ontology list as follows: 

```
ontology;irrigation;begin typing to refine list
```
  - `ontology` refers to the type of question
  - `irrigation` refers to the name of the resource located in the Our-Sci Gitlab [resources](https://gitlab.com/our-sci/resources) repository
  - `begin typing to refine list` is the text that will be displayed in the hint field on the mobile app.