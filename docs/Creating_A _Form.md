# Creating A Form

## Getting Started

The [Our Sci platform](https://app.our-sci.net/#/) uses [**ODK Build**](https://build.opendatakit.org/) to create forms. ODK Build is free to use, but you will need to create an account before you can get started.

There are numerous question types that are currently supported by the Our Sci platform. See the table below: 

|Type|Description|
|----|-----------|
|Text|Type in the answer in short answer form|
|Numeric|Enter a number|
|Date/Time|Enter date and time|
|Location|Uses the phones GPS to capture location data|
|Media|Used to run measurement scripts|
|Choose One|Provide a list of possible answers, user can select only 1 answer|
|Select Multiple|Provide a list of possible answers, user can select many answers|
|Group|Used to differentiate sections in a form, also used for features like anonymization|

## Create a New Form

1. In ODK Build, go to `File` and select `New Form`.
2. Name the form. Select `rename`, enter the form name and select `done`.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/bf7be96e6619d9d8c63c26ffea171277/image.png)

## Add Questions To a Form

1. Choose the question type from the menu at the bottom of the page. **Note**: Whenever you want to add a new question, just click on the question type that you want to add and a new question will be added to the bottom of the form.

2. Once you have selected the question type, the `Properties` for that question are visible in the right-hand sidebar.

3. Give the question a `Data Name`. This name is used to reference the data from the data tables, dashboards and measurement scripts. 

4. Give the question a `Label`. This label is what the user sees on the mobile app.

5. Provide a `hint` if necessary. An example hint may be the units for a measurements. **Example**: How long is the field; `hint`: meters.

6. If you chose *Choose One* or *Select Multiple*, you will need to enter all possible answers in the `Properties` sidebar:

     a. Scroll down to `Option 1`. 

     b. In the `English` box enter the answer as you want visible in the mobile app.

     c. In the `Underlying Value` box, enter the answer as you want it referenced in the data table, dashboards, and measurement scripts. 

     **Important Note**: You need to fill in both the `English` and `Underlying Value` boxes for each answer.
     
     d. Select `Add Option` to add another possible answer and repeat steps b and c until you have entered all of the possible answers. 

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/86b6d0bd9f5e2a241bf1ac359831b858/image.png)

## Changing the Order of Questions
The questions can be re-arranged by dragging and dropping questions in the order in which you want them to appear on the mobile app.

## Requiring a Question
You can set certain questions to be required. If someone tries to submit an instance of that form without answering a required question, a pop-up will appear in the mobile app letting the user know that we failed to answer a required question. However, the user can still choose to submit the survey without answering the required question.

To mark a question as required, check the `Required` box in the `Properties` sidebar (see above).

## Adding a Measurement Script

Measurement scripts can be added by using the *Media* question type. Measurement scripts can include scripts to run a measurement using an instrument (For example, taking a measurement with the **Bionutrient Sensor**), running a script that calculates new data from the answers of survey questions, or scripts that pull data from websites using API's.

To add a measurement script:

1. Create a *Media* question.

2. Give the measurement a *Data Name* and *Label* just like regular questions.

3. Get the ***id*** of the script you want to add from the scripts tab of the [web app](https://app.our-sci.net/#/script):

     a. Scroll through the list to find the script that you want to add. 

     **Note:** The script to run a basic measurement using the *Bionutrient Sensor* or *Our Sci Reflectometer* is `standard-5`.

     b. Click on the script.

     c. Copy the `id` of the script.

4. In the `Hint` box enter "Start Measurement" followed by a ";" and then id of the script you wish to add to your form (with no spaces after the ";"). For example: `Start Measurement;standard-5`.  

5. Change the `Kind` of media from the default (Image) to `Video` in the drop down menu. 

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/a5cbf9eeb035dcd54358f0f06fa46669/image.png)

## Uploading a Form
While forms are created in ODK build, they cannot be added directly to the web app from ODK Build. The form will need to be exported from ODK build and then uploaded to the web app.

1. In the `File` tab, `save` your form.
2. In the `File` tab, export your form by selecting `Export to XML`.
3. Go to the [web app](https://app.our-sci.net/#/).
4. In the left-hand sidebar, select `Create new...`.
5. Select `Survey` from the dropdown menu.
6. Drag and drop the XML file that you exported from ODK Build into the box or browse files to select the XML file.
7. Select `Upload Survey`. 

**Important Note**: Once you have completed uploading the form, it should be available in the mobile app in the `Forms` tab. Make sure your android device is connected to the internet to download a new form for the 1st time.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/4dadd019aa40a040f7ce6460b5ce230f/image.png)