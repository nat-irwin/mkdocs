# Organizations

## What is an organization

An **Organization** is a group of users that are conducting experiments together. By using organizations, a group of users can `Favorite` forms, add members and set permissions about who can add forms and members and who can view anonymized data. Additionally, organizations can use the [Contributing to an organization](https://nat-irwin.gitlab.io/mkdocs/Contributing_to_an_organization/) feature to manage forms in the **OurSciKit** app.

## Administrative Rights
  - Add/Edit members
  - Add/Edit `Favorites`
  - View all anonymized data within the organization


## Creating an organization
1. Go to the [web app](https://app.our-sci.net/#/) and sign in
2. In the left-hand sidebar, select `Create new...` and choose **Organization** from the drop down menu
3. Give your organization a name, ID, description and add content.
4. Select `Create`
5. Once you have created an organization you can `Edit` the organization to add members and `Favorite` forms.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/33fb4666fc23936d848306f0a474e0ff/image.png)

## Editing an organization
1. From the web app, select `Organizations` from the left-hand sidebar.
2. Select the organization you wish to edit from your list of **My Organizations**.
3. Select `Edit`.
4. You can add new members and favorite forms from the table at the bottom of the page.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/f67620d567611e682ffe5a20bc8258a3/image.png)

## Adding/Editing a member
1. Select the `Members` tab from the table at the bottom of the **Edit Organization** page.
2. Add a new member:

     a. Select `Add Member`

     b. Enter their email address in the box. If you want them to have **Admin** access, check the `Admin` box.

     c. Select `Submit`.
     
    **Note:** In order to add a member, they must have already signed in to the Our Sci platform, either the [web app](https://app.our-sci.net/#/) or any one of the [Our Sci family of apps](https://nat-irwin.gitlab.io/mkdocs/Getting_Started/).

3. Edit/remove a member:

     a. Select `EDIT` from the **Action** column for the member your wish to edit.

     b. Edit the members profile (email address, Display name, administrative rights, etc).

     c. To remove a member, select the `Delete` button.

     d. Select `Update`
     
4. Select `Update` once you have finished editing your organization.

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/07a4f1b74f7623694a952f15243b981b/image.png)

## Adding Favorite forms
1. Select the `Favorites` tab from the table at the bottom of the **Edit Organization** page.
2. Add a new Favorite:

     a. Select `Add Favorite`

     b. Choose what you are adding to your organization (a survey (form), dashboard or script) from the `Type` dropdown menu

     c. Choose the form by typing in the form name in the `Survey` box. The forms will auto-suggest once you start typing in the form name.

     d. Assign a description or priority (optional).

     e. Select `Sumbit`. 

3. Edit/remove a favorite

     a. Select `EDIT` from the **Action** column for the item your wish to edit.

     b. Edit the item

     c. To remove a favorite, select the `Delete` button.

     d. Select `Update`


![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/713317ac7a0a53969fddfc90d24c46b7/image.png)