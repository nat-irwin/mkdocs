# GPS Keep-Alive

If you will be using the GPS location frequently over a short time period (Ex: soil sampling to generate organic matter field maps) you can set your phone to keep its GPS locked to your current location. This will reduce the amount of time needed to capture your GPS location. 

#### Enable GPS Keep-Alive

  - From the home screen of the app (below left)
  - From the settings menu of the app (below, right)

![image](https://gitlab.com/our-sci/documentation/-/wikis/uploads/e9d487c932b5fd08365cc550f455086a/image.png)